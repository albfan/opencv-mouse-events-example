#!/usr/bin/env python

# USAGE
# ./click_and_crop.py --image jurassic_park_kitchen.jpg

# import the necessary packages
import argparse
import cv2

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False

print("Use Mouse to make a selection");
print("Use R to reset the selection");
print("Use C to crop the selection");

def click_and_crop(event, x, y, flags, param):
	# grab references to the global variables
	global refPt, cropping

	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt = [(x, y)]
		cropping = True

	# check to see if the left mouse button was released
	elif event == cv2.EVENT_LBUTTONUP:
		# record the ending (x, y) coordinates and indicate that
		# the cropping operation is finished
		refPt.append((x, y))
		cropping = False

		# draw a rectangle around the region of interest
		cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
		cv2.imshow("image", image)
	elif event == cv2.EVENT_MOUSEMOVE and flags == cv2.EVENT_FLAG_LBUTTON:
		clone = image.copy()
		cv2.rectangle(clone, refPt[0], (x, y), (0, 255, 0), 2)
		cv2.imshow("image", clone)

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

# load the image, clone it, and setup the mouse callback function
image = cv2.imread(args["image"])
clone = image.copy()

cv2.namedWindow("image")
cv2.setMouseCallback("image", click_and_crop)
cv2.imshow("image", image)

# keep looping until the 'q' key is pressed
while cv2.getWindowProperty("image", cv2.WND_PROP_VISIBLE) > 0:
	# display the image and wait for a keypress
	key = cv2.waitKey(1) & 0xFF

	# if the 'r' key is pressed, reset the cropping region
	if key == ord("r"):
		image = clone.copy()
		cv2.imshow("image", image)

	# if the 'c' key is pressed, show the crop
	elif key == ord("c"):
		if len(refPt) == 2:
			roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
			cv2.imshow("ROI", roi)
			while cv2.getWindowProperty("ROI", cv2.WND_PROP_VISIBLE) > 0:
				cv2.waitKey(50)

# close all open windows
cv2.destroyAllWindows()
