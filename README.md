# Mouse listener with OpenCV

![screencast](mouse-events.webm)

Improvements on pyimagesearch example to listen to mouse events

## Credits

- Adrian Rosebrock, Capturing mouse click events with Python and OpenCV, PyImageSearch, https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/, accessed on 21 July 2020
